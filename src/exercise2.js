const sum_of_elements = numbers => {
  if(numbers === [])
    return 0;
  return numbers.reduce((a, b) => a + b, 0);
}

const total_elements = numbers => {
  return numbers.length
}

const faverage = numbers => {
  if(!numbers)
    return 0;
  if(numbers && numbers.length === 0)
    return 0;
  return sum_of_elements(numbers) / total_elements(numbers)
};

module.exports = faverage;
