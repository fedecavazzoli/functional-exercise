const wordCount = sequence => {
  if(!sequence)
    return null;

  sequence = sequence.toLowerCase().split(' ').filter((element) => element !== '');
  if(sequence.length === 0)
    return null;
  
  return sequence.reduce((accumulator, currentValue) =>
    (accumulator[currentValue] = (accumulator[currentValue] || 0) + 1, accumulator), {});
};

module.exports = {
  wordCount,
};
